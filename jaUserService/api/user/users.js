const User = require('./user.js');

/**
 * Users class handles all User related operations
 * @type {{findAll: (function(): *), findUsersByEmail: (function(*=): *), insertUser: (function(*=): *), deleteUser: (function(*=): *), saveUser: (function(*=): *)}}
 */

let Users = {

    /**
     * Get a list of all available users
     * @returns {*}
     */

    getAll: () => {
        return User.scan().exec()
        .then((data)=>{
            console.log(data);
            return{
                statusCode: 200,
                body: JSON.stringify({
                    data: data
                })
            };
        }).catch((err) => {
            console.error(err);
            return{
                statusCode: 500,
                body: JSON.stringify({
                    message: "Unable to get user list",
                })
            };
        });
    },

     /**
     *  Find User by email address
     * @param email
     *  @returns {a|*|Promise<T | {message: string, error: string}>}
     */

    findUsersByEmail: email => {
        return User.query('email').eq(email).exec()
        .then((data) => {
            console.log(data);
            return {
                statusCode: 200,
                body: JSON.stringify({
                    data: data
                })
            };
        })
        .catch(err => {
            console.error(err);
            return {
                statusCode: 500,
                body: JSON.stringify({
                    message: "Unable to find the user",
                })
            };
        });
    },


    /**
     * Inserts a user into database
     * @param user
     *  @returns {a|*|Promise<T | {message: string, error: string}>}
     */

    insertUser: (user) => {
        let timestamp = new Date().getTime();
        user.updatedAt = timestamp;
        user.createdAt = timestamp;

        return new User(user).save()
        .then((data) => {
            return {
                statusCode: 200,
                body: JSON.stringify({
                    message: "User created",
                    data: data
                })
            };
        })
        .catch(err => {
            if (err) {
                console.error(err);
                return {
                    statusCode: "500",
                    body:JSON.stringify({
                        message: "Unable to insert the user",
                    })
                };
            }
        });
    },


    /**
     * Delete user from the database
     * @param email
     * @returns {a|*|Promise<T | {message: string, error: string}>}
     */

    deleteUser: (email) => {
        return User.delete({email: email})
        .then(data => {
            return {
                statusCode: "200",
                body: JSON.stringify({
                    message: "User Deleted",
                    data: data
                })
            }
        })
        .catch(err => {
            console.error(err);
            return {
                statusCode: "500",
                body: JSON.stringify({
                    message: "Unable to delete user",
                })
            };
        });
    },


    /**
     * Saves the user by Replacing the entire object
     * Gotta be careful not to submit a subset of the user, because in this particular case
     * the whole thing will be overwritten and and any values not in the passed object will be
     * lost.
     * @param user
     * @returns {a|*|Promise<T | {message: string, error: any}>}
     */

    saveUser: (user) => {
        user.updatedAt = new Date().getTime();
        return new User(user).save()
        .then(data => {
            return {
                statusCode: "200",
                body: JSON.stringify({
                    message: "User updated",
                    data: data
                })
            }
        })
        .catch(err => {
            console.error(err);
            return {
                statusCode: "500",
                body:JSON.stringify({
                    message: "Unable to update the user",
                })
            };
        });
    }
};

module.exports = Users;