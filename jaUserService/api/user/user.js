const validator = require('validator');
const dyno = require('dynamoose');
const Schema = dyno.Schema;

/**
 * This is the definition of the user schema.
 * To keep it simple I limited it to only email, first and last name.
 * @type {dyno.Schema}
 */

const userSchema = new Schema({
    email: {
        type: 'String',
        validate: value => {
            return validator.isEmail(value);
        },
        hashKey: true
    },

    firstName: {
        type: 'String',
        validate: value => {
            return !(value === "" || typeof value !== 'string');
        },
    },

    lastName: {
        type: 'String',
        validate: value => {
            return !(value === "" || typeof value !== 'string');
        }
    },
});

/**
 * This is a userModel instance declaration based on the user schema.
 * Only the userModel is being "exposed" via the module.exports as in the context of this
 * exercise there is no need for the access to the schema itself.
 */
const userModel = dyno.model('User', userSchema);



module.exports = userModel;