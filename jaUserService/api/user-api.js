'use strict';
const AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});
const users = require('./user/users.js');
//const validator = require('validator');


/**
 * usrAdd endpont that corresponds to path /users/add
 * @param event
 * @returns {Promise<void>}
 */

module.exports.add = async (event) => {
    console.log(event.body);
    let req = JSON.parse(event.body);
    return users.insertUser(req);
};

/**
 * usrUpdate endpont that corresponds to path /users/update
 * @param event
 * @returns {Promise<void>}
 */

module.exports.update = async (event) => {
    console.log(event.body);
    let req = JSON.parse(event.body);
    return users.saveUser(req);
};

/**
 * userFindBy endpont that corresponds to path /users/findby/:email
 * @param event
 * @returns {Promise<void>}
 */

module.exports.findby = async (event) => {
    let req = event.pathParameters;
    console.log("Email:" + req.email);
    return users.findUsersByEmail(req.email);
};

/**
 * userFind endpont that corresponds to path /users/findby/:email
 * @param event
 * @returns {Promise<void>}
 */

module.exports.all = async () => {
    return users.getAll();
};

/**
 * userDelete endpont that corresponds to path /users/delete
 * @param event
 * @returns {Promise<void>}
 */

module.exports.delete = async (event) => {
    console.log(event.body);
    let req = JSON.parse(event.body);
    return users.deleteUser(req.email);
};

