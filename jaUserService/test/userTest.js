'use strict';
// declare required component import references
const AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});
const mochaPlugin = require('serverless-mocha-plugin');
const expect = mochaPlugin.chai.expect;
const chaiHttp = require('chai-http');
const chai = require('chai');
chai.use(chaiHttp);
const User = require('../api/user/user.js');

// Declare lamdahost and path to the api
// I chose to do it this way, because I did not want to setup dynamo locally, and just deployed everything
// straight to AWS.
const lambdahost = 'https://nm1te8q7n1.execute-api.us-east-1.amazonaws.com';
const apipath = '/dev/users';

// I have created a small custom user object generator using randomapi, that
// generates a JSON object consistent with the schema I use for this exercise
const randomhost = 'https://randomapi.com';
const randompath = '/api/2ravcipd?key=0ADV-J7WQ-EB7L-QHV5';

// Declare function endpoints
const userAdd = apipath+'/add';
const userFindBy = apipath+'/findby/';
const userGetAll = apipath+'/all';
const userUpdate = apipath+'/update';
const userDelete = apipath+'/delete';

let allUsers = [];
let myUser = {};


/**
* For the sakes of the test and to demonstrate the use of generator function*
* I encapsulated the chai request into a function fetchAllUsers and declard it as a generator function with function*(){...}
* and instructed node to only execute chai.request and halt all other functions until the request has been completed.
* If you follow the console.log output you will notice that now done() function executes in a synchronous manner following
* fetchAllUsers().next() call.
*/
describe('userGetAll', () => {

    it('Must find all users in the database and store in allUsers array (Prep work for further testing)',
        function(done){
            let fetchAllUsers = function*() {
                console.log("Testing userGetAll");
                console.log("Requesting: " + lambdahost + userGetAll);
                yield chai.request(lambdahost)
                .get(userGetAll)
                .then((res) => {
                    allUsers = res.body.data;
                    expect(res.statusCode).to.be.equal(200);
                }).catch((err)=>{
                    console.error("Caught userGetAll Error:" + err);
                });
            };

            fetchAllUsers().next();
            console.log("Done Testing userGetAll");
            done();
        }
    );
});



describe('userAdd', () => {

    let deleteOldUsers = () =>{
        console.log("Deleting old users");
        if(allUsers.length > 0){
            User.batchDelete(allUsers)
                .then(result => {
                    console.log("Users deleted: " + JSON.stringify(result));
                    return result;
                });
        }else{
            console.log("Nothing to be deleted");
            return {};
        }
    };


    it('Must delete users from the previous test run', function(done){
        /**
         * In this case I use async await to ensure synchronous execution.
         */
        async function doDelete(){
            try{
                let result = await deleteOldUsers();
                await console.log("Finished deleting old users");
                await console.log("Deletion result is: " + JSON.stringify(result));
                await done();
            }catch(err){
                console.error(err);
                done()
            }
        }
        doDelete();
    });

    it('Must get fake data and insert into the db', function (done) {
        console.log("Testing userAdd");
        chai.request(randomhost)
        .get(randompath)
        .end((err, res) => {
            if(err){
                console.error("Error Occurred");
                console.error(err);
            }

            let payload = JSON.parse(res.text);
            myUser = payload.results[0];
            //console.log("Entering generated user:" + JSON.stringify(myUser));

            return chai.request(lambdahost)
            .post(userAdd)
            .send(myUser)
            .then((response)=>{
                //console.log("Insert Resulted:" + JSON.stringify(response.body));
                myUser = response.body.data;
                expect(response.statusCode).to.be.equal(200);
                expect(response.body.data).to.not.be.empty;
                console.log("Done testing userAdd");
                done();
            })
            .catch(err => {
                console.error(err);
                done();
            });
        });
    });
});

describe('userFindBy', () => {
    it('Must find user by email', function (done) {
        //console.log("Requesting: " + lambdahost + userFindBy + myUser.email);
        chai.request(lambdahost)
        .get(userFindBy + myUser.email)
        .then((res) => {
            //console.log("userFindBy Resulted:" + JSON.stringify(res.body.data));
            expect(res.statusCode).to.be.equal(200);
            done();
        })
        .catch((err)=>{
            console.error("Caught userFindAll Error:" + err);
            done();
        });

    });
});


describe('userUpdate', () => {
    it('should get fake data and update the user in the db', function (done) {
        chai.request(randomhost)
            .get(randompath)
            .end((err, res) => {
                if(err){
                    console.error("Error Occurred");
                    console.error(err);
                }
                let payload = JSON.parse(res.text);
                let user = payload.results[0];
                //console.log("Updating generated user:" + JSON.stringify(user));

                myUser.firstName = user.firstName;
                myUser.lastName = user.lastName;

                //console.log("Updating generated user:" + JSON.stringify(user) +
                //    "with new values:" + JSON.stringify(myUser));

                return chai.request(lambdahost)
                .put(userUpdate)
                .send(myUser)
                .then((response)=>{
                    //console.log("Update Resulted:" + JSON.stringify(response.body));
                    expect(response.statusCode).to.be.equal(200);
                    done();
                })
                .catch((err)=>{
                    console.log(err);
                    done();
                });
            });
    });
});



describe('userDelete', () => {
    it('should delete user by email', function (done) {
        chai.request(lambdahost)
        .delete(userDelete)
        .send({email: myUser.email})
        .then((response) => {
            //console.log("userDelete Resulted:" + JSON.stringify(response.body));
            expect(response.statusCode).to.be.equal(200);
            done();
        })
        .catch((err)=>{
            console.log(err);
            done();
        });
    });
});



