**Coding Test Interview for a position of Node.js developer at Orangetheory Fitness**

The goal of the exercise was to build restful API using AWS API gateway, DynamoDB and serverless using the following:

1. Use the following Open Source API: 
https://randomuser.me/
 
2. Create a CRUD API for member information. 
3. Use lambda Functions to build the API
4. Implement Unit Testing (mocha and chai) or the one you preferred
5. USE ECMA Script6  (arrow functions * functions /promises and Async Await)
6. Document the code using Swagger or Postman

Links
https://serverless.com/
https://www.getpostman.com/
https://swagger.io/


***Endpoints***

 *POST* - https://nm1te8q7n1.execute-api.us-east-1.amazonaws.com/dev/users/add

 *PUT*- https://nm1te8q7n1.execute-api.us-east-1.amazonaws.com/dev/users/update

 *GET*- https://nm1te8q7n1.execute-api.us-east-1.amazonaws.com/dev/users/findby/{email}

 *GET* - https://nm1te8q7n1.execute-api.us-east-1.amazonaws.com/dev/users/all

 *DELETE* - https://nm1te8q7n1.execute-api.us-east-1.amazonaws.com/dev/users/delete

***Lambda Functions:***

userAdd: jaUserService-dev-userAdd

userUpdate: jaUserService-dev-userUpdate

userFindBy: jaUserService-dev-userFindBy

userFindAll: jaUserService-dev-userFindAll

userDelete: jaUserService-dev-userDelete

*** Installation and running ***

git clone https://liquitive@bitbucket.org/liquitive/userpop.git





